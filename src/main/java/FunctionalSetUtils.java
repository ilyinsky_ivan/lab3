import java.util.function.Function;

public class FunctionalSetUtils {

    public static PurFuncSet<Integer> range(int start, int end) { return (x) -> x >= start && x <= end; }

    public static <T> PurFuncSet<T> empty() { return (x) -> x == null; }

    public static <T> PurFuncSet<T> singletonSet(T val) { return (x) -> x == val; }

    public static <T> PurFuncSet<T> union(PurFuncSet<T> s, PurFuncSet<T> t) { return (x) -> s.contains(x) || t.contains(x); }

    public static <T> PurFuncSet<T> intersect(PurFuncSet<T> s, PurFuncSet<T> t) { return (x) -> s.contains(x) && t.contains(x); }

    public static <T> PurFuncSet<T> diff(PurFuncSet<T> s, PurFuncSet<T> t) { return (x) -> s.contains(x) && !t.contains(x); }

    public static <T> PurFuncSet<T> filter(PurFuncSet<T> s, Predicate<T> p) { return (x) -> s.contains(x) && p.test(x); }

    public static <T> boolean forAll(PurFuncSet<Integer> s, Predicate<Integer> p) { return forAllRecursion(-1000, s, p); }

    public static <T> boolean exists(PurFuncSet<Integer> s, Predicate<Integer> p) { return !forAll(s, (x) -> !p.test(x)); }

    public static <T> PurFuncSet<Integer> map(PurFuncSet<Integer> s, Function<Integer, T> p) { return (y) -> exists(s, (x) -> y == p.apply(x)); }

    public static boolean forAllRecursion(int currantEl, PurFuncSet<Integer> s, Predicate<Integer> p){
        if (currantEl >= 1000)
            return true;
        if (s.contains(currantEl) && !p.test(currantEl))
            return false;
        else
            return forAllRecursion(++currantEl, s, p);
    }

    //для пулл реквеста
}