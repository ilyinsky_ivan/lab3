import com.sun.source.tree.AssertTree;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FunctionalSetUtilsTest {

    @Test
    void empty() {
        PurFuncSet<Integer> pfs = FunctionalSetUtils.empty();
        assertTrue(pfs.contains(null));
    }

    @Test
    void singletonSet() {
        PurFuncSet<Integer> pfs = FunctionalSetUtils.singletonSet(-1);
        assertTrue(pfs.contains(-1));
    }

    @Test
    void union() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(1, 5);
        PurFuncSet<Integer> pf2 = FunctionalSetUtils.range(-3, 0);
        PurFuncSet<Integer> pfs = FunctionalSetUtils.union(pf1, pf2);
        assertTrue(pfs.contains(3));
        assertTrue(pfs.contains(-1));
    }

    @Test
    void intersect() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(1, 5);
        PurFuncSet<Integer> pf2 = FunctionalSetUtils.range(4, 8);
        PurFuncSet<Integer> pfs = FunctionalSetUtils.intersect(pf1, pf2);
        assertTrue(pfs.contains(4));
        assertTrue(pfs.contains(5));
    }

    @Test
    void diff() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(3, 5);
        PurFuncSet<Integer> pf2 = FunctionalSetUtils.range(4, 6);
        PurFuncSet<Integer> pfs = FunctionalSetUtils.diff(pf1, pf2);
        assertTrue(pfs.contains(3));
    }

    @Test
    void filter() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(1, 5);
        PurFuncSet<Integer> pfs = FunctionalSetUtils.filter(pf1, (x) -> x >= 4);
        assertTrue(pfs.contains(4));
        assertTrue(pfs.contains(5));
    }

    @Test
    void forAll() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(1, 5);
        assertTrue(FunctionalSetUtils.forAll(pf1, (x) -> x >= 1));
    }

    @Test
    void exists() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(1, 5);
        assertTrue(FunctionalSetUtils.exists(pf1, (x) -> x >= 5));
    }

    @Test
    void map() {
        PurFuncSet<Integer> pf1 = FunctionalSetUtils.range(1, 5);
        PurFuncSet<Integer> pfs = FunctionalSetUtils.map(pf1, (x) -> x * 5);
        assertTrue(pfs.contains(10));
        assertTrue(pfs.contains(20));
    }
}